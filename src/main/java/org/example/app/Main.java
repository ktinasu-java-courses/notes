package org.example.app;

import com.google.gson.Gson;
import lombok.extern.slf4j.Slf4j;
import org.example.app.manager.UserManager;
import org.example.framework.di.Container;
import org.example.framework.di.processor.BeanProcessor;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.middleware.anon.AnonAuthMiddleware;
import org.example.framework.security.middleware.jsonbody.JSONBodyAuthNMiddleware;
import org.example.framework.security.proxy.Proxy;
import org.example.framework.server.annotation.Controller;
import org.example.framework.server.controller.ControllerRegisterer;
import org.example.framework.server.controller.method.handler.ReturnValueHandler;
import org.example.framework.server.controller.method.resolver.ArgumentResolver;
import org.example.framework.server.http.Server;
import org.example.template.JpaTransactionTemplate;

import java.util.Arrays;

@Slf4j
public class Main {
    public static void main(String[] args) {
        System.setProperty("javax.net.ssl.keyStore", "web-certs/server.jks");
        System.setProperty("javax.net.ssl.keyStorePassword", "passphrase");

        final Container container = new Container();
        container.register(Gson.class);
        container.register(new JpaTransactionTemplate());
        container.register("org.example");
        container.register((BeanProcessor) (bean, originalClazz, argumentTypes, arguments) -> {
            final boolean match = Arrays.stream(originalClazz.getDeclaredMethods())
                    .anyMatch(o -> o.isAnnotationPresent(HasRole.class));
            if (match) {
                final Proxy proxy = new Proxy();
                return proxy.securedProxy(bean, originalClazz, argumentTypes, arguments);
            }
            return bean;
        });
        container.register((BeanProcessor) (bean, originalClazz, argumentTypes, arguments) -> {
            final boolean match = Arrays.stream(originalClazz.getDeclaredMethods())
                    .anyMatch(o -> o.isAnnotationPresent(Audit.class));
            if (match) {
                final Proxy proxy = new Proxy();
                return proxy.auditedProxy(bean, originalClazz, argumentTypes, arguments);
            }
            return bean;
        });

        container.wire();

        final Gson gson = container.getBean(Gson.class);
        final UserManager userManager = container.getBean(UserManager.class);

        final Server server = Server.builder()
                .middleware(new JSONBodyAuthNMiddleware(gson, userManager))
                .middleware(new AnonAuthMiddleware())
                .argumentResolvers(container.getBeansByType(ArgumentResolver.class))
                .returnValueHandlers(container.getBeansByType(ReturnValueHandler.class))
                .router(new ControllerRegisterer().register(container.getBeansByAnnotation(Controller.class)))
                .build();

        final int port = 8443;
        try {
            server.start(port);
        } catch (Exception e) {
            log.error("can't serve", e);
        }
    }
}
