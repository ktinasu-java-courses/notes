package org.example.app.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "users")
@NamedQuery(
    name = UserEntity.FIND_ALL,
    query = "SELECT e FROM UserEntity e ORDER BY e.id"
)
@NamedQuery(
        name = UserEntity.FIND_BY_ID,
        query = "SELECT e FROM UserEntity e WHERE e.id = :id"
)
@NamedQuery(
        name = UserEntity.FIND_BY_LOGIN,
        query = "SELECT e FROM UserEntity e WHERE e.login = :login"
)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class UserEntity {
  public static final String FIND_ALL = "UserEntity.findAll";
  public static final String FIND_BY_ID = "UserEntity.findById";
  public static final String FIND_BY_LOGIN = "UserEntity.findByLogin";

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;
  @Column(unique = true, columnDefinition = "TEXT")
  private String login;
  @Column(columnDefinition = "TEXT")
  private String password;
}
