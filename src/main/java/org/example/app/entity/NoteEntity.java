package org.example.app.entity;

import lombok.*;

import javax.persistence.*;

@Entity
@Table(name = "notes")
@NamedQuery(
        name = NoteEntity.FIND_ALL,
        query = "SELECT e FROM NoteEntity e WHERE owner.login = :login ORDER BY e.id"
)
@NamedQuery(
        name = NoteEntity.FIND_BY_ID,
        query = "SELECT e FROM NoteEntity e WHERE e.id = :id AND owner.login = :login"
)
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@ToString
public class NoteEntity {
    public static final String FIND_ALL = "NoteEntity.findAll";
    public static final String FIND_BY_ID = "NoteEntity.findById";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    @Column(columnDefinition = "TEXT")
    private String content;
    private byte[] media;
    private boolean removed;
    @ManyToOne
    @JoinColumn(name = "user_id")
    private UserEntity owner;
}
