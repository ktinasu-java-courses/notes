package org.example.app.exception;

public class CantReadFilesException extends RuntimeException {
    public CantReadFilesException() {
    }

    public CantReadFilesException(String message) {
        super(message);
    }

    public CantReadFilesException(String message, Throwable cause) {
        super(message, cause);
    }

    public CantReadFilesException(Throwable cause) {
        super(cause);
    }

    public CantReadFilesException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
