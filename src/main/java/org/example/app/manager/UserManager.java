package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.entity.UserEntity;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.Audit;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.AuthenticationToken;
import org.example.framework.security.auth.Authenticator;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.security.exception.AuthenticationException;
import org.example.template.JpaTransactionTemplate;
import org.springframework.security.crypto.argon2.Argon2PasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class UserManager implements Authenticator {
    private final JpaTransactionTemplate template;
    private final PasswordEncoder encoder = new Argon2PasswordEncoder();

    public List<UsersGetAllRS> getAll(final int limit) {
        return template.executeInTransaction(em ->
                        em.createNamedQuery(UserEntity.FIND_ALL, UserEntity.class)
                                .setMaxResults(limit)
                                .getResultList()).stream()
                .map(o -> new UsersGetAllRS(o.getId(), o.getLogin()))
                .collect(Collectors.toList());
    }

    public UserGetByIdRS getById(final long id) {
        final UserEntity entity = template.executeInTransaction(em ->
                em.createNamedQuery(UserEntity.FIND_BY_ID, UserEntity.class)
                        .setParameter("id", id)
                        .getSingleResult());
        return new UserGetByIdRS(entity.getId(), entity.getLogin());
    }

    public UserRegisterRS register(final UserRegisterRQ requestDTO) {
        final String encodedPassword = encoder.encode(requestDTO.getPassword().trim().toLowerCase());
        final UserEntity entity = new UserEntity(
                0,
                requestDTO.getLogin().trim().toLowerCase(),
                encodedPassword);
        final UserEntity user = template.executeInTransaction(em -> {
            if (entity.getId() == 0) {
                em.persist(entity);
                return entity;
            }
            return em.merge(entity);
        });
        return new UserRegisterRS(user.getId(), user.getLogin());
    }

    @HasRole("USER")
    public UserRemoveByIdRS removeById(final long id, final UserRemoveByIdRQ requestDTO) {
        if (!SecurityContext.getPrincipal().getName().equals(requestDTO.getLogin())) {
            throw new AuthenticationException("Failed authentication");
        }
        template.executeInTransaction(em -> {
            final UserEntity ref = em.getReference(UserEntity.class, id);
            em.remove(ref);
            return null;
        });
        return new UserRemoveByIdRS("Successful deletion");
    }

    @Override
    @Audit
    public boolean authenticate(final AuthenticationToken requestDTO) {
        final Object login = requestDTO.getLogin();
        final String password = (String) requestDTO.getCredentials();

        final UserEntity userEntity = template.executeInTransaction(em ->
                em.createNamedQuery(UserEntity.FIND_BY_LOGIN, UserEntity.class)
                        .setParameter("login", login)
                        .getSingleResult());
        final String encodedPassword = userEntity.getPassword();
        return encoder.matches(password.trim().toLowerCase(), encodedPassword);
    }
}
