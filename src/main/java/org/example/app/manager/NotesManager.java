package org.example.app.manager;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.entity.NoteEntity;
import org.example.app.entity.UserEntity;
import org.example.app.exception.NotFoundItemException;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.annotation.HasRole;
import org.example.framework.security.auth.SecurityContext;
import org.example.framework.security.exception.AuthenticationException;
import org.example.template.JpaTransactionTemplate;

import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor
public class NotesManager {
    private final JpaTransactionTemplate template;

    @HasRole("USER")
    public List<NotesGetAllRS> getAll(final int limit, final NotesGetAllRQ requestDTO) {
        if (!SecurityContext.getPrincipal().getName().equals(requestDTO.getLogin())) {
            throw new AuthenticationException("Failed authentication");
        }
        return template.executeInTransaction(em ->
                        em.createNamedQuery(NoteEntity.FIND_ALL, NoteEntity.class)
                                .setParameter("login", requestDTO.getLogin())
                                .setMaxResults(limit)
                                .getResultList()).stream()
                .filter(o -> !o.isRemoved())
                .map(o -> new NotesGetAllRS(o.getId(),
                        o.getContent(),
                        o.getMedia()))
                .collect(Collectors.toList());
    }

    @HasRole("USER")
    public NoteGetByIdRS getById(final long id, final NoteGetByIdRQ requestDTO) {
        if (!SecurityContext.getPrincipal().getName().equals(requestDTO.getLogin())) {
            throw new AuthenticationException("Failed authentication");
        }
        final NoteEntity entity = template.executeInTransaction(em ->
                em.createNamedQuery(NoteEntity.FIND_BY_ID, NoteEntity.class)
                        .setParameter("id", id)
                        .setParameter("login", requestDTO.getLogin())
                        .getSingleResult());
        if (!entity.isRemoved()) {
            return new NoteGetByIdRS(entity.getId(),
                    entity.getContent(),
                    entity.getMedia());
        }
        throw new NotFoundItemException("Item not found");
    }

    @HasRole("USER")
    public NotesCreateRS create(final NotesCreateRQ requestDTO) {
        if (!SecurityContext.getPrincipal().getName().equals(requestDTO.getLogin())) {
            throw new AuthenticationException("Failed authentication");
        }
        final UserEntity userEntity = template.executeInTransaction(em ->
                em.createNamedQuery(UserEntity.FIND_BY_LOGIN, UserEntity.class)
                        .setParameter("login", requestDTO.getLogin())
                        .getSingleResult());
        final byte[] decodedBytes = Base64.getDecoder().decode(requestDTO.getMedia());
        final NoteEntity entity = new NoteEntity(0,
                requestDTO.getContent(),
                decodedBytes,
                false,
                userEntity);
        final NoteEntity note = template.executeInTransaction(em -> {
            if (entity.getId() == 0) {
                em.persist(entity);
                return entity;
            }
            return em.merge(entity);
        });
        return new NotesCreateRS(note.getId());
    }

    @HasRole("USER")
    public NoteRemoveByIdRS remove(final long id, final NoteRemoveByIdRQ requestDTO) {
        if (!SecurityContext.getPrincipal().getName().equals(requestDTO.getLogin())) {
            throw new AuthenticationException("Failed authentication");
        }
        final NoteEntity entity = template.executeInTransaction(em ->
                em.createNamedQuery(NoteEntity.FIND_BY_ID, NoteEntity.class)
                        .setParameter("id", id)
                        .setParameter("login", requestDTO.getLogin())
                        .getSingleResult());
        entity.setRemoved(true);
        final NoteEntity note = template.executeInTransaction(em -> {
            return em.merge(entity);
        });
        return new NoteRemoveByIdRS("Successful deletion");
    }

    @HasRole("USER")
    public NotesUpdateRS update(final long id, final NotesUpdateRQ requestDTO) {
        if (!SecurityContext.getPrincipal().getName().equals(requestDTO.getLogin())) {
            throw new AuthenticationException("Failed authentication");
        }
        final NoteEntity entity = template.executeInTransaction(em ->
                em.createNamedQuery(NoteEntity.FIND_BY_ID, NoteEntity.class)
                        .setParameter("id", id)
                        .setParameter("login", requestDTO.getLogin())
                        .getSingleResult());
        entity.setContent(requestDTO.getContent());
        final byte[] decodedBytes = Base64.getDecoder().decode(requestDTO.getMedia());
        entity.setMedia(decodedBytes);
        final NoteEntity note = template.executeInTransaction(em -> {
            return em.merge(entity);
        });
        return new NotesUpdateRS(entity.getId(),
                entity.getOwner().getLogin(),
                entity.getContent(),
                entity.getMedia());
    }
}
