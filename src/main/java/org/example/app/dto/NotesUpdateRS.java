package org.example.app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class NotesUpdateRS {
    private long id;
    private String login;
    private String content;
    private byte[] media;
}
