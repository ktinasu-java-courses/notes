package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.manager.NotesManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.http.HttpMethods;
import org.example.framework.server.annotation.*;

import java.util.List;

@Component
@Controller
@RequiredArgsConstructor
public class NotesController {
    public static final int LIMIT = 5;
    private final NotesManager manager;

    @RequestMapping(method = HttpMethods.GET,
            path = "^/notes/(?<id>\\d+)$")
    @ResponseBody
    public NoteGetByIdRS getById(@PathVariable("id") final long id,
                                 @RequestBody NoteGetByIdRQ requestDTO) {
        return manager.getById(id, requestDTO);
    }

    @RequestMapping(method = HttpMethods.GET, path = "^/notes$")
    @ResponseBody
    public List<NotesGetAllRS> getAll(@RequestBody NotesGetAllRQ requestDTO) {
        return manager.getAll(LIMIT, requestDTO);
    }

    @RequestMapping(method = HttpMethods.POST, path = "^/notes$")
    @ResponseBody
    public NotesCreateRS create(@RequestBody final NotesCreateRQ requestDTO) {
        return manager.create(requestDTO);
    }

    @RequestMapping(method = HttpMethods.PUT,
            path = "^/notes/(?<id>\\d+)$")
    @ResponseBody
    public NotesUpdateRS update(@PathVariable("id") final long id,
                                @RequestBody final NotesUpdateRQ requestDTO) {
        return manager.update(id, requestDTO);
    }

    @RequestMapping(method = HttpMethods.DELETE,
            path = "^/notes/(?<id>\\d+)$")
    @ResponseBody
    public NoteRemoveByIdRS delete(@PathVariable("id") final long id,
                                   @RequestBody final NoteRemoveByIdRQ requestDTO) {
        return manager.remove(id, requestDTO);
    }
}
