package org.example.app.controller;

import lombok.RequiredArgsConstructor;
import org.example.app.dto.*;
import org.example.app.manager.UserManager;
import org.example.framework.di.annotation.Component;
import org.example.framework.security.auth.LoginPasswordAuthenticationToken;
import org.example.framework.security.http.HttpMethods;
import org.example.framework.server.annotation.*;

import java.util.List;

@Component
@Controller
@RequiredArgsConstructor
public class UserController {
    public static final int LIMIT = 5;
    private final UserManager manager;

    @RequestMapping(method = HttpMethods.GET, path = "^/users/(?<id>\\d+)$")
    @ResponseBody
    public UserGetByIdRS getById(@PathVariable("id") final long id) {
        return manager.getById(id);
    }

    @RequestMapping(method = HttpMethods.DELETE, path = "^/users/(?<id>\\d+)$")
    @ResponseBody
    public UserRemoveByIdRS removeById(@PathVariable("id") final long id,
                                       @RequestBody final UserRemoveByIdRQ requestDTO) {
        return manager.removeById(id, requestDTO);
    }

    @RequestMapping(method = HttpMethods.GET, path = "^/users$")
    @ResponseBody
    public List<UsersGetAllRS> getAll() {
        return manager.getAll(LIMIT);
    }

    @RequestMapping(method = HttpMethods.POST, path = "^/register$")
    @ResponseBody
    public UserRegisterRS register(@RequestBody final UserRegisterRQ requestDTO) {
        return manager.register(requestDTO);
    }

    @RequestMapping(method = HttpMethods.POST, path = "^/auth")
    @ResponseBody
    public boolean authenticate(@RequestBody final LoginPasswordAuthenticationToken requestDTO) {
        return manager.authenticate(requestDTO);
    }
}
